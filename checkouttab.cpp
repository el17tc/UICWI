#include <QtWidgets>

#include "gitpp7.h"
#include "checkouttab.h"

CheckoutTab::CheckoutTab(QWidget *parent)
    : QWidget(parent)
{
  branchListBox = new QListWidget;
  branch_count = 0;

  try{
    GITPP::REPO r;
    for(auto i : r.branches()){
      branch_count++;
      branchListBox->insertItem(0,i.name().c_str());
  	}
  }
  catch(GITPP::EXCEPTION_CANT_FIND){
  }

  //create button
  checkout_button = new QPushButton("Checkout", this);
  checkout_button->setFixedSize(QSize(100, 30));
  connect(checkout_button, SIGNAL (released()), this, SLOT (checkoutBranch()));

  //check for branches
  if (branch_count == 0){
    checkout_button->setEnabled(false);
    branchListBox->insertItem(0,"You have no branches");
  }

  //create layout
  QVBoxLayout *layout = new QVBoxLayout;
  layout->addWidget(branchListBox);
  layout->addWidget(checkout_button);
  setLayout(layout);
}

void CheckoutTab::checkoutBranch()
{
  if (branchListBox->currentIndex().isValid()){
    current_branch = branchListBox->currentItem()->text().toStdString();

    GITPP::REPO r;
    r.checkout(current_branch);

    //create pop up window on successfull branch checkout
    msgbox = new QMessageBox(this);
    msgbox->setWindowTitle("Git interface");
    msgbox->setText("Successfully changed branches");
    msgbox->exec();
    qApp->exit(1337);
  }
  else{
    //create pop up window on unsuccessfull branch checkout
    QMessageBox::warning(this, tr("Git interface"),
      tr("Failed to change branches"));
  }
}
