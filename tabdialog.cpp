#include <QtWidgets>
#include "gitpp7.h"
#include "tabdialog.h"
#include "checkouttab.h"
#include "foldertab.hpp"
#include "keywordsearch.hpp"
#include "configtab.h"
#include "commit.h"



	GitInterface::GitInterface(QWidget *parent)
: QDialog(parent)
{
	setWindowFlags(Qt::Window
			| Qt::WindowMinimizeButtonHint
			| Qt::WindowMaximizeButtonHint
			| Qt::WindowCloseButtonHint);

	char a = 1;
	try
	{
		GITPP::REPO();
	}
	catch(...)
	{
		a = 0;
	}
	tabWidget = new QTabWidget;

	if (a)
	{

		tabWidget->addTab(new CheckoutTab(), tr("Branch navigation"));
		tabWidget->addTab(new FolderTab(), tr("Select repo"));
		tabWidget->addTab(new CommitHistoryTab(), tr("Commit history"));
		tabWidget->addTab(new KeywordSearch(), tr("Keyword search"));
		tabWidget->addTab(new ConfigTab(), tr("Config settings"));
	}
	else{
		tabWidget->addTab(new FolderTab(), tr("Select repo"));
	}


	QVBoxLayout *mainLayout = new QVBoxLayout;
	mainLayout->addWidget(tabWidget);
	setLayout(mainLayout);

	setWindowTitle(tr("Git interface"));
}
