#include "configtab.h"
#include "varcontainer.h"
#include "gitpp7.h"
#include <iostream>
#include <sstream>
#include <vector>
#include <algorithm>

ConfigTab::ConfigTab(QWidget *parent) : QWidget(parent)
{
    QVBoxLayout *rightColumn = new QVBoxLayout();
    QWidget *dummyRight = new QWidget();
    dummyRight->setLayout(rightColumn);
    rightColumn->setSizeConstraint(QLayout::SetFixedSize);
    QLabel *changeVarLabel = new QLabel("Change config variable:");
    currentVarLabel = new QLabel("");

    QHBoxLayout *oldVarLayout = new QHBoxLayout();
      oldLabel = new QLabel("old value:");
      QLabel *subvarLabel = new QLabel();
      oldVarLayout->addWidget(oldLabel);
      oldVarLayout->addWidget(subvarLabel);

    QVBoxLayout *newVarLayout = new QVBoxLayout();
      QLabel *newLabel = new QLabel("new value:");
      newVarInput = new QLineEdit();
      confirmButton = new QPushButton("Confirm");
      confirmButton->setEnabled(false);
      newVarLayout->addWidget(newLabel);
      newVarLayout->addWidget(newVarInput);
      QHBoxLayout *confirmLayout = new QHBoxLayout();
        confirmLayout->addWidget(confirmButton);
        confirmLayout->addStretch(2);
      newVarLayout->addLayout(confirmLayout);

    QVBoxLayout *warningMessage = new QVBoxLayout();
        warningIMG = new QLabel("");
        QPixmap Warnpix("./icons/warningIcon.png");
        warningIMG->setPixmap(Warnpix.scaled(50,50));

      warningLabel2 = new QLabel("This error can be changed. Error messages will wrap to the next line and should be returned from a validation function which uses exceptions to discern.");
      warningIMG->hide();
      warningLabel2->hide();
      warningLabel2->setWordWrap(true);
      warningMessage->addWidget(warningIMG);
      warningMessage->addWidget(warningLabel2);
      warningMessage->setEnabled(false);

    QVBoxLayout *topHalf = new QVBoxLayout();
    topHalf->addWidget(changeVarLabel);
    topHalf->addWidget(currentVarLabel);
    topHalf->addLayout(oldVarLayout);
    topHalf->addLayout(newVarLayout);
    QWidget *dummyWid2 = new QWidget(this);
    dummyWid2->setLayout(topHalf);
    dummyWid2->setMinimumHeight(dummyWid2->sizeHint().height());
    rightColumn->addWidget(dummyWid2);
    rightColumn->addLayout(warningMessage);

    //initialise the left column
    QVBoxLayout *leftColumn = new QVBoxLayout();

    QHBoxLayout *searchBox = new QHBoxLayout();
      varSearch = new QLineEdit();
      searchButton = new QPushButton();
      QPixmap searchPix("./icons/searchIcon.png");
      QIcon buttonIcon(searchPix.scaled(30,30));
      searchButton->setIcon(buttonIcon);
      searchButton->setFixedWidth(25);
      searchBox->addWidget(varSearch);
      searchBox->addWidget(searchButton);
      searchBox->addStretch(1);

    configScroll = new QScrollArea();

    configs = new QVBoxLayout();
    initConfigVars(configs); // pull the config variables from the git.config file and parse them into the layout
    //configs->setContentsMargins(1,1,1,1);

    dummyWidget = new QWidget(this);
    dummyWidget->setLayout(configs);
    dummyWidget->setMinimumWidth(dummyWidget->sizeHint().width()+30);
    configScroll->setWidget(dummyWidget);
    //configScroll->setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );

    leftColumn->addLayout(searchBox);
    leftColumn->addWidget(configScroll);

    newVarInput->setEnabled(false);

    layout = new QHBoxLayout();

    layout->addLayout(leftColumn);
    layout->addWidget(dummyRight);

    setLayout(layout);

    connect(confirmButton, SIGNAL(clicked()),
            this, SLOT(changeVariable()));

    connect(searchButton, SIGNAL(clicked()),
            this, SLOT(searchVariables()));


}

void ConfigTab::setRightLayout(varContainer* origin) {
    currentVarContainer = origin;
    warningIMG->hide();
    warningLabel2->hide();
    if(newVarInput->isEnabled() == false) {
      newVarInput->setEnabled(true);
      confirmButton->setEnabled(true);
    }
    QString newText = QString::fromStdString(origin->_fullVarName);
    currentVarLabel->setText(newText);
    std::string valStr = "old: ";
    QString valText = QString::fromStdString(valStr + origin->_varValue);
    oldLabel->setText(valText);
}

bool is_number(std::string str) {
    for (int currentChar : str) {
        if (!isdigit(currentChar)) {
            return false;
        }
    }
    return true;
}

int to_bool(std::string var, std::string &errMsg) {
    std::string s = var;
    int len = s.length();
    for (int i = 0; i < len; i++) {
        s[i] = tolower(s[i]);
    }

    if (s == "true" || s == "t") {
        return 1;
    }
    if ((is_number(s)) && (stoi(s) > 0)) {
        errMsg = "Variable can be True or False. Positive integers default to True";
        return 1;
    }
    if (s == "false" || s == "f") {
      return 0;
    }
    errMsg = "Variable can be True or False. Invalid inputs default to false";
    return 0;
}

void ConfigTab::changeError(std::string errorMessage) {
  warningIMG->show();
  QString errstring = QString::fromStdString(errorMessage);
  warningLabel2->setText(errstring);
  warningLabel2->show();
  QMessageBox::warning(this, tr("Invalid Input"), errstring);
}

void ConfigTab::changeVariable() {

    int oldWidth = currentVarContainer->varLabel->width();

    /*
    qDebug() << "currentVarContainer->varLabel->width() = " << currentVarContainer->varLabel->width() << ", hint = " << currentVarContainer->varLabel->sizeHint().width();
    qDebug() << "currentVarContainer->width = " << currentVarContainer->width() << ", hint = " << currentVarContainer->sizeHint().width();
    qDebug() << "dummyWidget->width = " << dummyWidget->width() << ", hint = " << dummyWidget->sizeHint().width();
    qDebug() << "configScroll->width = " << configScroll->width() << ", hint = " << configScroll->sizeHint().width();
    qDebug() << "==================";
    */

    QString newVar = newVarInput->text();
    std::string cppstring = newVar.toUtf8().constData();
    std::string subStr = currentVarContainer->_fullVarName.substr(currentVarContainer->_fullVarName.find(".")+1);


    //if invalid, display warning
    if (subStr == "repositoryformatversion") {
        std::string errMsg = "repository format version is a critical variable which cannot be changed (anything other than 0 ruins the repository)";
        changeError(errMsg);
        newVarInput->setText("");
        newVarInput->setEnabled(false);
        confirmButton->setEnabled(false);
        return;
    }

    if (currentVarContainer->_fullVarName == "push.default") {
        if (!(cppstring == "simple" ||
            cppstring == "matching" ||
            cppstring == "current")) {
            std::string errMsg = "push.default can only take 'simple', 'matching' or 'current'. Variable unchanged.";
            changeError(errMsg);
            newVarInput->setText("");
            newVarInput->setEnabled(false);
            confirmButton->setEnabled(false);
            return;
        }
    }
    if (currentVarContainer->_varValue == "true" ||
        currentVarContainer->_varValue == "false") {
        std::string errMsg = "none";
        int boolInt = to_bool(cppstring, errMsg); // because got_config_parse_bool keeps crashing it
        if (boolInt) {
          cppstring = "true";
        } else {
          cppstring = "false";
        }
        if (errMsg != "none")
            changeError(errMsg);
    }

    try {
        GITPP::REPO r;
        auto c=r.config();
        c[currentVarContainer->_fullVarName] = cppstring;
        //e.g. c[user.email]
    } catch (GITPP::EXCEPTION_CANT_FIND const& e) {
        qDebug() << "Warning: git repository not found, no real variable changed.";
    }

    currentVarContainer->_varValue = cppstring;
    std::string labelString = subStr + " = " + currentVarContainer->_varValue;
    QString Qvar = QString::fromStdString(labelString);
    currentVarContainer->varLabel->setText(Qvar);
    newVarInput->setText("");
    newVarInput->setEnabled(false);
    confirmButton->setEnabled(false);

    int newWidth = currentVarContainer->varLabel->sizeHint().width();
    int newDummyWidth = dummyWidget->width() + (newWidth-oldWidth);
    dummyWidget->setMinimumWidth(newDummyWidth);

    //currentVarContainer->varLabel->resize(currentVarContainer->varLabel->sizeHint().width(), currentVarContainer->varLabel->height());
    //currentVarContainer->resize(currentVarContainer->sizeHint().width(), currentVarContainer->height());

    /* // debugging purposes
    qDebug() << "currentVarContainer->varLabel->width() = " << currentVarContainer->varLabel->width() << ", hint = " << currentVarContainer->varLabel->sizeHint().width();
    qDebug() << "currentVarContainer->width = " << currentVarContainer->width() << ", hint = " << currentVarContainer->sizeHint().width();
    qDebug() << "dummyWidget->width = " << dummyWidget->width() << ", hint = " << dummyWidget->sizeHint().width();
    qDebug() << "configScroll->width = " << configScroll->width() << ", hint = " << configScroll->sizeHint().width();
    */
}

void ConfigTab::initConfigVars(QVBoxLayout* layout, std::string searchStr) {
    try {
        GITPP::REPO r;
        auto c = r.config();
        varContainer *varSub = nullptr;
        std::vector<std::string> vectorOfHeads; // this whole crap is to get them to display in order
        for (auto i : c) {
            //find the head of the variable
            std::stringstream ss (i.name());
            std::string head;
            getline(ss, head, '.');
            //std::cout << head << "\n";
            if (vectorOfHeads.size() == 0 || (std::find(vectorOfHeads.begin(), vectorOfHeads.end(), head) == vectorOfHeads.end())) {
              //std::cout << "that's true!\n";
              // if the head string is not in the vector for holding head strings
              vectorOfHeads.push_back(head); // add it to the vector (automatically assigns more memory)
            }
        }
        for (auto currentHead : vectorOfHeads) {
            QString QHead = QString::fromStdString(currentHead + ": ");
            QLabel *varHeadLabel = new QLabel(QHead);
            varHeadLabel->setStyleSheet("border-top-width: 1px; border-top-style: solid; border-radius: 0px;");
            layout->addWidget(varHeadLabel);
            for (auto i : c) {
                GITPP::CONFIG::ITEM n = c[i.name()];
                //find the head of the variable
                std::stringstream ss (i.name());
                std::string head;
                getline(ss, head, '.');
                std::string subStr = i.name().substr(i.name().find(".")+1);
                if ((currentHead == head) &&
                   ((subStr.find(searchStr)!=std::string::npos) || (searchStr == ""))) {
                    varSub = new varContainer(this, i.name(), n.value(), this);
                    //std::cout << "i.name() = " << i.name() << " | n.value() = " << n.value() << "\n";
                    layout->addWidget(varSub);
                }
            }
        }
    } catch (GITPP::EXCEPTION_CANT_FIND const& e) {
        qDebug() << "Cannot initialise config variables: No .git found in the current directory";
    }
}

void ConfigTab::searchVariables() {
  newVarInput->setText("");
  newVarInput->setEnabled(false);
  confirmButton->setEnabled(false);

  QWidget *tempWidge = new QWidget(this);
  tempWidge->setLayout(configs);
  QVBoxLayout *tempLayout = new QVBoxLayout();

  initConfigVars(tempLayout, varSearch->text().toUtf8().constData());
  dummyWidget->setLayout(tempLayout);
  configs = tempLayout;

  delete tempWidge;
}
