#include <QtWidgets>

class KeywordSearch : public QWidget
{
  Q_OBJECT

public:
  explicit KeywordSearch(QWidget *parent = nullptr);

private:

  QLineEdit *searchInputBox;
  QListWidget *searchResultBox;
  QPushButton *searchButton;

private slots:
    void keywordSearch();
};
