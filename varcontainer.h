#ifndef VARCONTAINER_H
#define VARCONTAINER_H

#include <QObject>
#include <QWidget>
#include <QPushButton>
#include <QLabel>
#include <QHBoxLayout>


class varContainer : public QWidget
{
    Q_OBJECT
public:
    explicit varContainer(QWidget *parent = nullptr, std::string fullVarName = "default",
                          std::string varValue = "default", QWidget* topParent = nullptr);

public:
    QWidget *mainWindow;
    QLabel *varLabel;
    QPushButton *editButton;
    std::string _fullVarName; // e.g. "user.name"
    std::string _varValue; // e.g. "el17tc"

signals:
    void editClicked(varContainer*);

public slots:
    void clickedSlot();
};

#endif // VARCONTAINER_H
